<?php

require_once(__DIR__."/../vendor/autoload.php");

use Cart\Controllers\HomePageController;
use Cart\Core\Application;


$app = Application::getInstance();

$app::$router->get('/',[HomePageController::class,'index']);

$app->run();

//require ("src/Config/bootstrap.php");
//
//use Cart\Classes\Action;
//use Cart\Classes\Cart;
//use Cart\Classes\DataParser;
//
//$dataParser = new DataParser("./data.txt");
//$cart = new Cart([],[],0);
//
//$products = $dataParser->extractProducts();
//$actions = $dataParser->extractActions();
//$discounts = $dataParser->extractDiscounts();
//
//print_r($actions);
//
//
//foreach ($actions as $action)
//{
//    /** @var Action $action */
//    switch ($action->getAction()){
//        case "add":
//
//            break;
//        case "remove":
//            break;
//    }
//}

