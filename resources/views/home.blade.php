@extends('layout')

@section('body')

    <div class="row">
        <div class="col-4">
            <h2>Products</h2>
            <ul class="list-group">
                @foreach($products as $product)
                    <li class="list-group-item">Name: {{$product->getName()}} / Price: {{$product->getPrice()}} / Available Quantity: {{$product->getAvailableQuantity()}}</li>
                @endforeach
            </ul>
        </div>
        <div class="col-8">
            <h2>Cart</h2>
            <ul class="list-group">
                @foreach($cart->getCartItems() as $cartItem)
                    <li class="list-group-item">Name: {{$cartItem->getProduct()->getName()}} / Price: {{$cartItem->getProduct()->getPrice()}} / Quantity: {{$cartItem->getQuantity()}}</li>
                @endforeach
                <li class="list-group-item d-flex justify-content-end">
                    Price: {{$cart->getTotalPrice()}}
                </li>
                <li class="list-group-item d-flex justify-content-end">
                    Discount: {{$cart->getDiscountApplied()}}
                </li>
                <li class="list-group-item d-flex justify-content-end">
                    Total: {{$cart->getTotalPriceWithDiscount()}}
                </li>
            </ul>
        </div>
    </div>

@endsection
