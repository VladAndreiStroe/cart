<?php

namespace Cart\Core;

class Application
{
    public static Router $router;
    public static Request $request;
    public static Response $response;
    public static ?Application $app = null;

    private function __construct()
    {
    }

    public static function getInstance():Application
    {
        if(self::$app === null){
            self::$app = new Application();

            self::$request = new Request();
            self::$response = new Response();
            self::$router = new Router(self::$request, self::$response);
        }

        return self::$app;
    }

    public static function run()
    {
        echo self::$router->resolve();
    }
}