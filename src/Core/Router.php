<?php

namespace Cart\Core;

use eftec\bladeone\BladeOne;

class Router
{
    public Request $request;
    public Response $response;
    protected array $routes = [];
    protected BladeOne $bladeOne;

    private string $viewsPath = __DIR__."/../../resources/views";
    private string $cachePath = __DIR__."/../../resources/cache";

    /**
     * @param Request $request
     * @param Response $response
     */
    public function __construct(Request $request, Response $response)
    {
        $this->request = $request;
        $this->response = $response;
        $this->bladeOne = new BladeOne($this->viewsPath,$this->cachePath,BladeOne::MODE_DEBUG);
    }


    public function get($path, $callback)
    {
        $this->routes['get'][$path] = $callback;
    }

    public function post($path, $callback)
    {
        $this->routes['post'][$path] = $callback;
    }

    /**
     * @throws \Exception
     */
    public function resolve():string
    {
        $path = $this->request->getPath();
        $method = $this->request->getMethod();

        $callback = $this->routes[$method][$path] ?? false;

        if($callback === false){
            $this->response->setStatusCode(404);
            return $this->renderView('errors.404');
        }

        if(is_string($callback)){
           return $this->renderView($callback);
        }

        if(is_array($callback)){
            $controller = new $callback[0]();
            return call_user_func([$controller,$callback[1]]);
        }

        return call_user_func($callback);
    }

    /**
     * @throws \Exception
     */
    public function renderView(string $view, array $args = []):string
    {
       return $this->bladeOne->run($view,$args);
    }
}