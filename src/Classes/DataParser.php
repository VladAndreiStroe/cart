<?php

namespace Cart\Classes;

class DataParser
{
    private string $filePath;
    private array|bool $rawData;

    /**
     * @param string $filePath
     */
    public function __construct(string $filePath)
    {
        $this->filePath = $filePath;

        $rawData = $this->readFile();

        if($rawData !== false){
            $this->setRawData($rawData);

            // To not keep in memory rawData twice
            unset($rawData);

            foreach($this->rawData as &$row){
                trim($row);
            }
        }
    }

    /**
     * @return string|bool
     */
    private function readFile():array|bool
    {
        if(!empty($this->filePath)){
            return file($this->filePath, FILE_IGNORE_NEW_LINES);
        }

        return false;
    }

    public function extractProducts():array|null
    {
        $products = null;

        foreach($this->rawData as $index => $value)
        {
            $explodedRow = explode(" ",$value);

            if($explodedRow[0] === Product::PLURAL)
            {
                $internalIndex = $index + 1;

                for($i = $internalIndex; $i < count($this->rawData); $i++){
                    if(!empty($this->rawData[$i])){
                        $productData = explode(' ',$this->rawData[$i]);
                        $products[] = new Product($productData[0],$productData[1],$productData[2],$productData[3]);
                    }
                    else
                        break;
                }
            }
        }

        return $products;
    }

    public function extractActions():array|null
    {
        $actions = null;

        foreach($this->rawData as $index => $value)
        {
            $explodedRow = explode(" ",$value);

            if($explodedRow[0] === Action::PLURAL)
            {
                $internalIndex = $index + 1;

                for($i = $internalIndex; $i < count($this->rawData); $i++){
                    if(!empty($this->rawData[$i])){
                        $actionData = explode(' ',$this->rawData[$i]);
                        $actions[] = new Action($actionData[0],$actionData[1],$actionData[2]);
                    }
                    else
                        break;
                }
            }
        }

        return $actions;
    }

    public function extractDiscounts():array|null
    {
        $discounts = null;

        foreach($this->rawData as $index => $value)
        {
            $explodedRow = explode(" ",$value);

            if($explodedRow[0] === Discount::PLURAL)
            {
                $internalIndex = $index + 1;

                for($i = $internalIndex; $i < count($this->rawData); $i++){
                    if(!empty($this->rawData[$i])){
                        $discountData = explode(' ',$this->rawData[$i]);
                        $discounts[] = new Discount($discountData[0],$discountData[1]);
                    }
                    else
                        break;
                }
            }
        }

        return $discounts;
    }

    /**
     * @return string
     */
    public function getFilePath(): string
    {
        return $this->filePath;
    }

    /**
     * @param string $filePath
     */
    public function setFilePath(string $filePath): void
    {
        $this->filePath = $filePath;
    }

    /**
     * @return array|bool
     */
    public function getRawData(): array|bool
    {
        return $this->rawData;
    }

    /**
     * @param array|bool $rawData
     */
    public function setRawData(array|bool $rawData): void
    {
        $this->rawData = $rawData;
    }

}