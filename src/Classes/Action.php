<?php

namespace Cart\Classes;

class Action
{

    private string $action;
    private int $quantity;
    private int $productId;

    public const PLURAL = "Actions";

    /**
     * @param string $action
     * @param int $quantity
     * @param int $productId
     */
    public function __construct(string $action, int $quantity, int $productId)
    {
        $this->action = $action;
        $this->quantity = $quantity;
        $this->productId = $productId;
    }

    /**
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }

    /**
     * @param string $action
     */
    public function setAction(string $action): void
    {
        $this->action = $action;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity(int $quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * @return int
     */
    public function getProductId(): int
    {
        return $this->productId;
    }

    /**
     * @param int $productId
     */
    public function setProductId(int $productId): void
    {
        $this->productId = $productId;
    }


}