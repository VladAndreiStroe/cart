<?php

namespace Cart\Classes;

class Discount
{

    private float $minPrice;
    private float $percent;

    public const PLURAL = "Discounts";

    /**
     * @param float $minPrice
     * @param float $percent
     */
    public function __construct(float $minPrice, float $percent)
    {
        $this->minPrice = $minPrice;
        $this->percent = $percent;
    }

    /**
     * @return float
     */
    public function getMinPrice(): float
    {
        return $this->minPrice;
    }

    /**
     * @param float $minPrice
     */
    public function setMinPrice(float $minPrice): void
    {
        $this->minPrice = $minPrice;
    }

    /**
     * @return float
     */
    public function getPercent(): float
    {
        return $this->percent;
    }

    /**
     * @param float $percent
     */
    public function setPercent(float $percent): void
    {
        $this->percent = $percent;
    }


}