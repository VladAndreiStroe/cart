<?php

namespace Cart\Classes;

use Cart\Core\Application;

abstract class BaseController
{
    public function renderView($view,$args)
    {
        return Application::$router->renderView($view,$args);
    }
}