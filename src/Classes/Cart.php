<?php

namespace Cart\Classes;

class Cart
{
    private array $cartItems;
    private array $discounts;
    private float $totalPrice;
    private float $discountApplied;
    private float $totalPriceWithDiscount;

    /**
     * @param array $cartItems
     * @param array $discounts
     * @param float $totalPrice
     */
    public function __construct(array $cartItems, array $discounts, float $totalPrice)
    {
        $this->cartItems = $cartItems;
        $this->discounts = $discounts;
        $this->totalPrice = $totalPrice;
    }

    public function addCartItem(CartItem $cartItem)
    {
        $productId = $cartItem->getProduct()->getId();

        if(count($this->cartItems) > 0){
            foreach ($this->cartItems as $cartCartItem){
                /** @var $cartCartItem CartItem */
                if($cartCartItem->getProduct()->getId() === $productId){
                    $cartCartItem->setQuantity($cartCartItem->getQuantity() + $cartItem->getQuantity());
                    return;
                }
            }
        }

        $this->cartItems[] = $cartItem;
    }

    public function removeCartItem(CartItem $cartItem)
    {
        $productId = $cartItem->getProduct()->getId();

        foreach ($this->cartItems as $cartCartItem) {
            /** @var $cartCartItem CartItem */
            if ($cartCartItem->getProduct()->getId() === $productId) {
                $cartCartItem->setQuantity($cartCartItem->getQuantity() - $cartItem->getQuantity());
            }
        }

    }

    public function processCart()
    {
        foreach ($this->cartItems as $index => $cartItem)
        {
            /** @var CartItem $cartItem */
            if($cartItem->getQuantity() <= 0){
                unset($this->cartItems[$index]);
            }
            else{
                $this->setTotalPrice($this->getTotalPrice() + $cartItem->getTotalPrice());
            }
        }

        foreach($this->discounts as $discount)
        {
            /** @var Discount $discount */
            $totalPrice = $this->getTotalPrice();

            if($discount->getMinPrice() < $totalPrice)
            {
                $this->setDiscountApplied($discount->getPercent());
            }
        }

        $this->applyDiscount();

        reset($this->cartItems);
    }

    private function applyDiscount()
    {
        $this->setTotalPriceWithDiscount($this->getTotalPrice() - ($this->getTotalPrice() * (1/$this->getDiscountApplied())));
    }

    /**
     * @return array
     */
    public function getCartItems(): array
    {
        return $this->cartItems;
    }

    /**
     * @param array $cartItems
     */
    public function setCartItems(array $cartItems): void
    {
        $this->cartItems = $cartItems;
    }

    /**
     * @return array
     */
    public function getDiscounts(): array
    {
        return $this->discounts;
    }

    /**
     * @param array $discounts
     */
    public function setDiscounts(array $discounts): void
    {
        $this->discounts = $discounts;
    }

    /**
     * @return float
     */
    public function getTotalPrice(): float
    {
        return $this->totalPrice;
    }

    /**
     * @param float $totalPrice
     */
    public function setTotalPrice(float $totalPrice): void
    {
        $this->totalPrice = $totalPrice;
    }

    /**
     * @return float
     */
    public function getDiscountApplied(): float
    {
        return $this->discountApplied;
    }

    /**
     * @param float $discountApplied
     */
    public function setDiscountApplied(float $discountApplied): void
    {
        $this->discountApplied = $discountApplied;
    }

    /**
     * @return float
     */
    public function getTotalPriceWithDiscount(): float
    {
        return (float) number_format($this->totalPriceWithDiscount,2,'.','');
    }

    /**
     * @param float $totalPriceWithDiscount
     */
    public function setTotalPriceWithDiscount(float $totalPriceWithDiscount): void
    {
        $this->totalPriceWithDiscount = $totalPriceWithDiscount;
    }




}