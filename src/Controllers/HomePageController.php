<?php

namespace Cart\Controllers;

use Cart\Classes\BaseController;
use Cart\Classes\Action;
use Cart\Classes\Cart;
use Cart\Classes\CartItem;
use Cart\Classes\DataParser;
use Cart\Classes\Product;

class HomePageController extends BaseController
{
    public function index()
    {
        $dataParser = new DataParser("./data.txt");
        $cart = new Cart([],[],0);

        $products = $dataParser->extractProducts();
        $actions = $dataParser->extractActions();
        $discounts = $dataParser->extractDiscounts();

        foreach($actions as $action){

            /** @var Action $action */
            switch ($action->getAction()){
                case "add":
                    $array = array_filter($products, function ($product) use ($action) {
                        return $product->getId() === $action->getProductId();
                    });
                    /** @var Product $product */
                    $product = reset($array);
                    $cart->addCartItem(new CartItem($product,$action->getQuantity()));
                    break;
                case "remove":
                    $array = array_filter($products, function ($product) use ($action) {
                        return $product->getId() === $action->getProductId();
                    });
                    /** @var Product $product */
                    $product = reset($array);
                    $cart->removeCartItem(new CartItem($product,$action->getQuantity()));
                    break;
            }

        }

        $cart->setDiscounts($discounts);
        $cart->processCart();

        return $this->renderView('home',[
            'products' => $products,
            'discounts' => $discounts,
            'cart' => $cart
        ]);
    }
}